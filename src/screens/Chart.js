import React, { Component } from 'react';
import { Row, Col } from 'react-bootstrap';

import Search from '../components/Search';
import List from '../components/List';

class Chart extends Component {
  render() {
    return (
      <Row>
        <Col>
          <Search />
          <List />
        </Col>
      </Row>
    );
  }
}

export default Chart;
