import _ from 'lodash';

export const searchResults = store => {
  return _.map(store.results, result =>
    _.pick(_.mapKeys(result, (value, key) => _.last(_.split(key, ' '))), ['symbol', 'name', 'region', 'currency'])
  );
};
