import {
  SEND_SEARCH_REQUEST,
  RECEIVE_SEARCH_RESULT,
  REQUEST_TIME_SERIES_DAILY_ADJUSTED,
  RECEIVE_TIME_SERIES_DAILY_ADJUSTED
} from '../actions';

const initialState = {
  searching: false,
  keyword: '',
  results: [],
  symbol: null,
  data: {}
};

export default function(state = initialState, action) {
  switch (action.type) {
    case SEND_SEARCH_REQUEST:
      return { ...state, keyword: action.keyword, searching: true };
    case RECEIVE_SEARCH_RESULT:
      return { ...state, results: action.results, searching: false };
    case REQUEST_TIME_SERIES_DAILY_ADJUSTED:
      return { ...state, symbol: action.symbol, searching: true };
    case RECEIVE_TIME_SERIES_DAILY_ADJUSTED:
      return { ...state, data: action.data, searching: false };
    default:
      return state;
  }
}
