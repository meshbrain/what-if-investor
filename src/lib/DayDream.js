import axios from 'axios';

class DayDream {
  constructor(apiKey) {
    this.apiKey = apiKey;
  }

  search(keyword) {
    return axios.get(
      `https://www.alphavantage.co/query?function=SYMBOL_SEARCH&keywords=${keyword}&apikey=${this.apiKey}`
    );
  }

  get(symbol) {
    return axios.get(
      `https://www.alphavantage.co/query?function=TIME_SERIES_DAILY_ADJUSTED&symbol=${symbol}&outputsize=full&apikey=${
        this.apiKey
      }`
    );
  }
}

export default new DayDream(process.env.REACT_APP_ALPHAVANTAGE_KEY);
