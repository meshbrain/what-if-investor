import _ from 'lodash';
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Table, Button } from 'react-bootstrap';

import { selectStock } from '../actions';
import { searchResults } from '../selectors';

class List extends Component {
  constructor(props) {
    super(props);

    this.handleClick = this.handleClick.bind(this);
  }

  handleClick(_, symbol) {
    this.props.selectStock(symbol);
  }

  render() {
    if (_.size(this.props.results) > 0) {
      return (
        <div>
          <Table responsive>
            <tbody>
              {_.map(this.props.results, (scrip, index) => (
                <tr key={index}>
                  <td>{_.get(scrip, 'symbol')}</td>
                  <td>{_.get(scrip, 'name')}</td>
                  <td>{_.get(scrip, 'region')}</td>
                  <td>{_.get(scrip, 'currency')}</td>
                  <td>
                    <Button variant="link" onClick={event => this.handleClick(event, _.get(scrip, 'symbol'))}>
                      Select
                    </Button>
                  </td>
                </tr>
              ))}
            </tbody>
          </Table>
        </div>
      );
    } else {
      return null;
    }
  }
}

export default connect(
  state => ({ results: searchResults(state) }),
  { selectStock }
)(List);
