import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Form, Col } from 'react-bootstrap';

import { searchStock } from '../actions';

class Search extends Component {
  constructor(props) {
    super(props);

    this.handleKeywordChange = this.handleKeywordChange.bind(this);
  }

  handleKeywordChange(event) {
    // TODO: debounce
    this.props.searchStock(event.target.value);
  }

  render() {
    return (
      <div>
        <Form>
          <Form.Row>
            <Col>
              <Form.Control
                placeholder="Start typing a stock name"
                value={this.props.keyword}
                onChange={this.handleKeywordChange}
              />
            </Col>
          </Form.Row>
        </Form>
        {/* {!_.isEmpty(this.state.currentScrip) && (
          <Card>
            <Card.Body>
              <Card.Title>{_.get(this.state.currentScrip, '1. symbol')}</Card.Title>
              <Card.Subtitle className="mb-2 text-muted">{_.get(this.state.currentScrip, '4. region')}</Card.Subtitle>
              <Card.Text>{_.get(this.state.currentScrip, '2. name')}</Card.Text>
            </Card.Body>
          </Card>
        )} */}
      </div>
    );
  }
}

export default connect(
  state => ({ keyword: state.keyword }),
  { searchStock }
)(Search);
