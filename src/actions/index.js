import _ from 'lodash';
import dayDreamer from '../lib/DayDream';

export const SEND_SEARCH_REQUEST = 'SEND_SEARCH_REQUEST';
export function sendSearchRequest(keyword) {
  return { type: SEND_SEARCH_REQUEST, keyword };
}

export const RECEIVE_SEARCH_RESULT = 'RECEIVE_SEARCH_RESULT';
export function receiveSearchResult(results) {
  return { type: RECEIVE_SEARCH_RESULT, results };
}

export function searchStock(keyword) {
  return function(dispatch) {
    dispatch(sendSearchRequest(keyword));

    if (1 < _.size(keyword)) {
      return dayDreamer
        .search(keyword)
        .then(response => dispatch(receiveSearchResult(_.get(response, 'data.bestMatches'))))
        .catch(error => {
          console.error('error', error);
          return dispatch(receiveSearchResult([]));
        });
    } else {
      return dispatch(receiveSearchResult([]));
    }
  };
}

export const REQUEST_TIME_SERIES_DAILY_ADJUSTED = 'REQUEST_TIME_SERIES_DAILY_ADJUSTED';
export function requestTimeSeriesDailyAdjusted(symbol) {
  return { type: REQUEST_TIME_SERIES_DAILY_ADJUSTED, symbol };
}

export const RECEIVE_TIME_SERIES_DAILY_ADJUSTED = 'RECEIVE_TIME_SERIES_DAILY_ADJUSTED';
export function receiveTimeSeriesDailyAdjusted(data) {
  return { type: RECEIVE_TIME_SERIES_DAILY_ADJUSTED, data };
}

export function selectStock(symbol) {
  return function(dispatch) {
    dispatch(requestTimeSeriesDailyAdjusted(symbol));

    return dayDreamer
      .get(symbol)
      .then(response => dispatch(receiveTimeSeriesDailyAdjusted(_.get(response, 'data'))))
      .catch(error => {
        console.error('error', error);
        return dispatch(receiveTimeSeriesDailyAdjusted({}));
      });
  };
}
