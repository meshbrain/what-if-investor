import React, { Component } from 'react';
import { Container } from 'react-bootstrap';

import Chart from './screens/Chart';

class App extends Component {
  render() {
    return (
      <Container>
        <Chart />
      </Container>
    );
  }
}

export default App;
