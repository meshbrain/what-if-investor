import thunkMiddleware from 'redux-thunk';
import { createLogger } from 'redux-logger';
import { createStore, applyMiddleware } from 'redux';

import rootReducer from '../reducers';

export default createStore(rootReducer, applyMiddleware(thunkMiddleware, createLogger()));
